package dev.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import dev.dao.UserRepository;
import dev.model.User;

import java.util.Arrays;
import java.util.List;
//import java.util.Objects;

@Controller
public class AuthController {
    private static final Logger log = Logger.getLogger(AuthController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    public AuthController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @RequestMapping(value = "/auth/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }

    @RequestMapping(value = "/registry", method = RequestMethod.GET)
    public ModelAndView addUser(@ModelAttribute User user){
        ModelAndView mav = new ModelAndView();
        List<String> roles = Arrays.asList("admin", "user", "anonymous");
        String role = "";
        mav.addObject("role", role);
        mav.addObject("roles", roles);
        mav.setViewName("registry");
        return mav;
    }


    @RequestMapping(value = "/registry", method = RequestMethod.POST)
    public ModelAndView addUser(@ModelAttribute User user, Model model, String role){
        role = "user";
        user.setRole("ROLE_".concat(role.toUpperCase()));
        Md5PasswordEncoder encoder = new Md5PasswordEncoder();
        user.setPassword(encoder.encodePassword(user.getPassword(),""));
        ModelAndView mav = new ModelAndView();
        boolean error = false;
        mav.setViewName("redirect:/auth/login");
        try{
            userRepository.save(user);
        }
        catch(Exception e){
            List<String> roles = Arrays.asList("admin", "user", "anonymous");
            mav.addObject("roles", roles);
            error = true;
            mav.setViewName("/registry");
        }
        finally {
            mav.addObject("error", error);
            return mav;
        }
    }
}