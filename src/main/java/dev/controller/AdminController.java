package dev.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import dev.dao.UserRepository;
import dev.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;


@Controller
public class AdminController {

    private static final Logger log = Logger.getLogger(AdminController.class);


    @Autowired
    private UserRepository userRepository;


    @RequestMapping(value = "/admin/", method = RequestMethod.GET)
    public ModelAndView index(){
        ModelAndView mav = new ModelAndView();
        org.springframework.security.core.userdetails.User authUser = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        dev.model.User u = userRepository.findByLogin(authUser.getUsername());
        String name = u.getName().concat(" ").concat(u.getSecondname());
        mav.addObject("name", name);
        mav.setViewName("admin/index");
        return mav;
    }

    private ModelAndView getModelView(String model, JpaRepository repository){
        ModelAndView mav = new ModelAndView();
        List<Model> obj = (List<Model>) repository.findAll();
        mav.addObject(model, obj);
        mav.setViewName("admin/".concat(model));
        return mav;
    }

    @RequestMapping(value = "/admin/{model}", method = RequestMethod.GET)
    public ModelAndView AdminMappingModel(@PathVariable(value="model") String model){
        ModelAndView mav = new ModelAndView();
        if (model.equals("users")) {
            mav = getModelView(model, userRepository);
        }
        else {
            mav.setViewName("404");
        }
        return mav;
    }

    @RequestMapping(value = "/admin/addUser", method = RequestMethod.GET)
    public ModelAndView addUser(@ModelAttribute User user){
        ModelAndView mav = new ModelAndView();
        List<String> roles = Arrays.asList("admin", "user", "anonymous");
        String role = "";
        mav.addObject("role", role);
        mav.addObject("roles", roles);
        mav.setViewName("admin/addUser");
        return mav;
    }


    @RequestMapping(value = "/admin/addUser", method = RequestMethod.POST)
    public ModelAndView addUser(@ModelAttribute User user, Model model, String role){
        user.setRole("ROLE_".concat(role.toUpperCase()));
        Md5PasswordEncoder encoder = new Md5PasswordEncoder();
        user.setPassword(encoder.encodePassword(user.getPassword(),""));
        ModelAndView mav = new ModelAndView();
        boolean error = false;
        mav.setViewName("redirect:/admin/users");
        try{
            userRepository.save(user);
        }
        catch(Exception e){
            List<String> roles = Arrays.asList("admin", "user", "anonymous");
            mav.addObject("roles", roles);
            error = true;
            mav.setViewName("/admin/addUser");
        }
        finally {
            mav.addObject("error", error);
            return mav;
        }
    }

    @RequestMapping(value = "/admin/editUser", method = RequestMethod.GET, params = {"id"})
    public ModelAndView editUser(@ModelAttribute User user, final HttpServletRequest req) {
        final Integer userId = Integer.valueOf(req.getParameter("id"));
        if (!userRepository.existsById(userId)){
            return new ModelAndView("404");
        }
        User userItem = (User) userRepository.findById(userId);
        userItem.setRole(userItem.getRole().substring(5,userItem.getRole().length()).toLowerCase());
        ModelAndView mav = new ModelAndView();
        String pass = "";
        List<String> roles = Arrays.asList("admin", "user", "anonymous");
        String role = "";
        mav.addObject("role", role);
        mav.addObject("roles", roles);
        mav.addObject("user", userItem);
        mav.addObject("pass", pass);
        mav.setViewName("admin/editUser");
        return mav;
    }

    @RequestMapping(value = "/admin/editUser", method = RequestMethod.POST)
    public ModelAndView editUser(@ModelAttribute User user, String pass, String role){
        user.setRole("ROLE_".concat(role.toUpperCase()));
        User oldUser = userRepository.findById(user.getId());
        if(pass.isEmpty()){
            user.setPassword(oldUser.getPassword());
        }else{
            Md5PasswordEncoder encoder = new Md5PasswordEncoder();
            user.setPassword(encoder.encodePassword(pass, ""));
        }
        ModelAndView mav = new ModelAndView();
        boolean error = false;
        mav.setViewName("redirect:/admin/users");
        try{
            userRepository.save(user);
        }
        catch(Exception e){
            List<String> roles = Arrays.asList("admin", "user", "anonymous");
            mav.addObject("roles", roles);
            error = true;
            mav.setViewName("/admin/editUser");
        }
        finally {
            mav.addObject("error", error);
            return mav;
        }

    }

    @RequestMapping(value = "/admin/deleteUser", method = RequestMethod.GET)
    public ModelAndView deleteUser(@ModelAttribute User user){
        long id = user.getId();
        if (!userRepository.existsById(id)){
            return new ModelAndView("404");
        }
        log.info(id);
        User userItem = (User) userRepository.findById(id);
        userRepository.delete(userItem);
        return new ModelAndView("redirect:/admin/users");
    }

    @RequestMapping(value="/admin/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/auth/login";
    }
}
