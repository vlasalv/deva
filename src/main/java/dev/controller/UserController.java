package dev.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import dev.dao.UserRepository;
import dev.model.User;
import dev.service.SecurityServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Iterator;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserRepository userRepository;

    private static final Logger logger = LoggerFactory.getLogger(SecurityServiceImpl.class);


    @RequestMapping(value = "/user/", method = RequestMethod.GET)
    public ModelAndView index(@ModelAttribute User user){
        ModelAndView mav = new ModelAndView();
        org.springframework.security.core.userdetails.User authUser = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        dev.model.User u = userRepository.findByLogin(authUser.getUsername());
        List<User> obj = (List<User>) userRepository.findAll();
        User item;
        Integer temp = 0;
        int count = 0;
        for (Iterator<User> i = obj.iterator(); i.hasNext();) {
            item = i.next();
            temp += (int)item.getAge();
            count++;
        }
        temp /= count;

        mav.addObject("averageAge",temp);
        mav.addObject("users",obj);
        mav.setViewName("/user/index");
        return mav;
    }

    @RequestMapping(value="/user/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/auth/login";
    }
}
